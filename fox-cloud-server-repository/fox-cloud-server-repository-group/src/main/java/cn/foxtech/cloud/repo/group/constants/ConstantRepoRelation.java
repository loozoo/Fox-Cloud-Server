package cn.foxtech.cloud.repo.group.constants;

import cn.foxtech.cloud.core.constant.Constant;

public class ConstantRepoRelation extends Constant {
    public static final String field_collection_name = "edgeRepoGroupRelation";

    public static final String field_name = "name";

    public static final String value_direct_user2group = "user2group";
    public static final String value_direct_group2user = "group2user";
}