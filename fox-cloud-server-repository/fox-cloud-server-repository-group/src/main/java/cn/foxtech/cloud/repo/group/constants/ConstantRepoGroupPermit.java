package cn.foxtech.cloud.repo.group.constants;

public class ConstantRepoGroupPermit {
    public static final String permit_group_delete = "group:delete";
    public static final String permit_group_create = "group:create";
}
