package cn.foxtech.cloud.repo.group.constants;

import cn.foxtech.cloud.core.constant.Constant;

public class ConstantRepoGroup extends Constant {
    public static final String field_collection_name = "edgeRepoGroup";

    public static final String field_id = "id";
    public static final String field_ids = "ids";

    public static final String field_group_name = "groupName";
    public static final String field_owner_id = "ownerId";

    public static final String field_member = "member";

    public static final String field_group_description = "description";

    public static final String value_public_group_name = "public";
    public static final String value_public_group_id = "public";
}
