package cn.foxtech.cloud.repo.group.constants;

public class ConstantRepoRole {
    public static final String role_admin = "admin";
    public static final String role_fox_edge_public = "fox-edge-public";
    public static final String role_fox_cloud_view = "fox-cloud-view";
    public static final String role_enterprise_user = "Enterprise-User";



}
