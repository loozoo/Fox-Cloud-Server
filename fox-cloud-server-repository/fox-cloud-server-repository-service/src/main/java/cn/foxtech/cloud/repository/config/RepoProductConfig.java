package cn.foxtech.cloud.repository.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"cn.foxtech.cloud.repo.product"})
public class RepoProductConfig {
}
