package cn.foxtech.cloud.repo.comp.files.constants;

public class ConstantRepoCompVer {
    public static final String field_stage = "stage";
    public static final String field_new_stage = "newStage";
    public static final String value_stage_master = "master";
    public static final String value_stage_release = "release";
    public static final String value_stage_develop = "develop";
    public static final String value_stage_feature = "feature";
}
