package cn.foxtech.cloud.repo.comp.files.constants;

import cn.foxtech.cloud.core.constant.Constant;

public class ConstantRepoComp extends Constant {
    public static final String field_collection_name = "edgeRepoComp";

    public static final String field_id = "id";

    public static final String field_uuid = "uuid";

    public static final String field_model_name = "modelName";
    public static final String field_model_names = "modelNames";
    public static final String field_model_type = "modelType";
    public static final String field_model_version = "modelVersion";
    public static final String field_owner_id = "ownerId";
    public static final String field_weight = "weight";

    public static final String field_group_id = "groupId";
    public static final String field_group_name = "groupName";

    public static final String field_component = "component";


    public static final String field_manufacturer = "manufacturer";

    public static final String field_device_type = "deviceType";

    public static final String field_description = "description";

    public static final String field_key_word = "keyWord";

    public static final String field_version = "version";

    public static final String field_value_model_type_decoder = "decoder";
    public static final String field_commit_key = "commitKey";

}
