package cn.foxtech.cloud.repo.product.constants;

import cn.foxtech.cloud.core.constant.Constant;

public class ConstantRepoProductComp extends Constant {
    public static final String field_collection_name = "edgeRepoProduct";

    public static final String field_product_id = "productId";

    public static final String field_uuid = "uuid";

    public static final String field_model_type = "modelType";

    public static final String field_model_name = "modelName";

    public static final String field_model_version = "modelVersion";
}
