package cn.foxtech.cloud.repo.product.constants;

import cn.foxtech.cloud.core.constant.Constant;

public class ConstantRepoProduct extends Constant {
    public static final String field_collection_name = "edgeRepoProduct";

    public static final String field_uuid = "uuid";

    public static final String field_manufacturer = "manufacturer";
    public static final String field_device_type = "deviceType";

    public static final String field_image = "image";

    public static final String field_url = "url";

    public static final String field_tags = "tags";

    public static final String field_description = "description";

    public static final String field_comps = "comps";

    public static final String field_weight = "weight";

    public static final String field_key_word = "keyWord";
}
