package cn.foxtech.cloud.repo.comp.script.constants;

public class ConstantRepoCompScriptOperate {
    public static final String field_version_id = "versionId";
    public static final String field_operate_name = "operateName";

    public static final String field_operate_id = "operateId";

    public static final String field_device_type = "deviceType";
    public static final String field_manufacturer = "manufacturer";
    public static final String field_comp_id = "compId";

    public static final String field_engine_type = "engineType";
    public static final String field_engine_param = "engineParam";
    public static final String field_extend_param = "extendParam";

    public static final String field_operate_mode = "operateMode";
    public static final String field_operate_modes = "operateModes";
    public static final String field_data_type = "dataType";
    public static final String field_service_type = "serviceType";
    public static final String field_polling = "polling";
    public static final String field_timeout = "timeout";

    public static final String field_create_time = "createTime";
    public static final String field_update_time = "updateTime";
}
