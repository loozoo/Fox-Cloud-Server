package cn.foxtech.cloud.repo.comp.script.constants;

import cn.foxtech.cloud.core.constant.Constant;

public class ConstantRepoCompScript extends Constant {
    public static final String field_collection_name = "edgeRepoCompScript";

    public static final String field_id = "id";
    public static final String field_manufacturer = "manufacturer";

    public static final String field_device_type = "deviceType";

    public static final String field_description = "description";

    public static final String field_owner_id = "ownerId";

    public static final String field_group_id = "groupId";

    public static final String field_group_name = "groupName";

    public static final String field_keyword = "keyWord";

    public static final String field_commit_key = "commitKey";


}