package cn.foxtech.cloud.repo.comp.script.constants;

import cn.foxtech.cloud.core.constant.Constant;

public class ConstantRepoCompScriptVersion extends Constant {
    public static final String field_collection_name = "edgeRepoCompScriptVersion";
    public static final String field_id = "id";
    public static final String field_script_id = "scriptId";
    public static final String field_script_ids = "scriptIds";

    public static final String field_comp_id = "compId";
    public static final String field_author = "author";
    public static final String field_description = "description";
    public static final String field_operates = "operates";
    public static final String field_create_time = "createTime";
    public static final String field_update_time = "updateTime";
    public static final String field_keyword = "keyWord";
}